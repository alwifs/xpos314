create database XPOS_314
use XPOS_314

create table TblCategory(
Id int primary key identity (1,1),
NameCategory varchar (50) not null,
Description varchar (100) null,
IsDelete bit null,
CreateBy int not null,
CreateDate datetime not null,
UpdateBy int null,
UpdateDate datetime null
)

drop table TblProduct

alter table TblProduct alter column Price decimal (18, 4)
select * from TblProduct
	
delete from TblProduct where Id=7

create table TblVariant(
Id int primary key identity (1,1),
IdCategory int not null,
NameVariant varchar (50) not null,
Description varchar (100) null,
IsDelete bit null,
CreateBy int not null,
CreateDate datetime not null,
UpdateBy int null,
UpdateDate datetime null
)

create table TblProduct(
Id int primary key identity (1,1),
IdVariant int not null,
NameProduct Varchar (100) not null,
Price decimal (18,0) not null,
Stock int not null,
Image varchar (max) null,
IsDelete bit null,
CreateBy int not null,
CreateDate datetime not null,
UpdateBy int null,
UpdateDate datetime null
)


create table TblOrderHeader(
Id int primary key identity(1,1),
CodeTransaction nvarchar(20) not null,
IdCostumer int not null,
Amount decimal(18,2) not null,
TotalQty int not null,
IsCheckout bit not null,
IsDelete bit null,
CreateBy int not null,
CreateDate datetime not null,
UpdateBy int null,
UpdateDate datetime null
)

create table TblOrderDetail(
Id int primary key identity(1,1),
IdHeader int not null,
IdProduct int not null,
Qty int not null,
SumPrice decimal(18,2) not null,
IsDelete bit null,
CreateBy int not null,
CreateDate datetime not null,
UpdateBy int null,
UpdateDate datetime null
)

create table TblRole(
Id int primary key identity (1,1),
RoleName varchar (50) null,
IsDelete bit null,
CreateBy int not null,
CreateDate datetime not null,
UpdateBy int null,
UpdateDate datetime null
)

create table TblCostumer(
Id int primary key identity(1,1),
NameCostumer nvarchar(50) not null,
Email nvarchar(50) not null,
Password nvarchar(50) not null,
Address nvarchar(100) not null,
Phone nvarchar(15) not null,
IdRole int null,
IsDelete bit not null,
CreateBy int not null,
CreateDate datetime not null,
UpdateBy int null,
UpdateDate datetime null
)