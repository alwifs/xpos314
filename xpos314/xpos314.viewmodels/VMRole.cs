﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xpos314.viewmodels
{
	internal class VMRole
	{
		public int Id { get; set; }
		public string? RoleName { get; set; }
		public bool? IsDelete { get; set; }
		public int CreateBy { get; set; }
		public DateTime CreateDate { get; set; }
		public int? UpdateBy { get; set; }
		public DateTime? UpdateDate { get; set; }
	}
}
