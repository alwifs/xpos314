﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using xpos314.viewmodels;
using xpos314.web.Services;

namespace xpos314.web.Controllers
{
	public class AuthController : Controller
	{
		private AuthService authService;
		VMResponse respon = new VMResponse();

		public AuthController(AuthService _authService)
		{
			this.authService = _authService;
		}
		public IActionResult Login()
		{
			return PartialView();
		}
		public IActionResult Logout()
		{
			HttpContext.Session.Clear();
			return RedirectToAction("Index", "Home");
		}
		public async Task<JsonResult> LoginSubmit(string email, string password)
		{

			VMCostumer costumer = await authService.CheckLogin(email, password);

			if (costumer != null)
			{
				respon.Message = $"Hallo, {costumer.NameCostumer} Welcome to XPOS";
				HttpContext.Session.SetString("NameCostumer", costumer.NameCostumer);
				HttpContext.Session.SetInt32("IdCostumer", costumer.Id);
				HttpContext.Session.SetInt32("IdRole", costumer.IdRole ?? 0);


			}
			else
			{
				respon.Message = $"Oops, {email} not found or password is wrong, please check it!";
			}
			return Json(new { dataRespon = respon });
		}



	}
}
