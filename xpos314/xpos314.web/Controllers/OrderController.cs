﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using xpos314.viewmodels;
using xpos314.web.Services;

namespace xpos314.web.Controllers
{
    public class OrderController : Controller
    {
        private ProductService productService;
        private OrderService orderService;
        private int IdUser = 1;

        public OrderController(ProductService _productService, OrderService _orderService)
        {
            this.productService = _productService;
            this.orderService = _orderService;
        }
        public async Task<IActionResult> Catalog(VMSearchPage dataSearch)
        {
            List<VMTblProduct> dataProduct = await productService.GetAllData();

            dataSearch.MinAmount = dataSearch.MinAmount == null ? decimal.MinValue : dataSearch.MinAmount;
            dataSearch.MaxAmount = dataSearch.MaxAmount == null ? decimal.MaxValue : dataSearch.MaxAmount;
            
            if(dataSearch.NameProduct != null)
            {
                dataProduct = dataProduct.Where(a => a.NameProduct == dataSearch.NameProduct).ToList();
            }
            if (dataSearch.MinAmount != null && dataSearch.MaxAmount != null)
            {
                dataProduct = dataProduct.Where(a => a.Price >= dataSearch.MinAmount && a.Price <= dataSearch.MaxAmount).ToList();
            }
            VMOrderHeader dataHeader = HttpContext.Session.GetComplexData<VMOrderHeader>("ListCart");

            if(dataHeader == null)
            {
                dataHeader= new VMOrderHeader();
                dataHeader.ListDetails = new List<VMOrderDetail>();
            }
            var ListDetail = JsonConvert.SerializeObject(dataHeader.ListDetails);
            ViewBag.dataHeader = dataHeader;
            ViewBag.dataDetail = ListDetail;
           
            ViewBag.Search = dataSearch;
            ViewBag.CurrentPageSize = dataSearch.CurrentPageSize;

            return View(PaginationList<VMTblProduct>.CreateAsync(dataProduct, dataSearch.PageNumber ?? 1, dataSearch.pageSize ?? 3));
        }

        [HttpPost]
        public JsonResult SetSession (VMOrderHeader dataHeader)
        {
            HttpContext.Session.SetComplexData("ListCart", dataHeader);
            return Json("");
        }
        public JsonResult RemoveSession()
        {
            HttpContext.Session.Remove("ListCart");
            return Json("");
        }
        public IActionResult OrderPreview(VMOrderHeader dataHeader)
        {
            return PartialView(dataHeader);
        }
        public IActionResult SearchMenu()
        {
            return PartialView();
        }
        public async Task <JsonResult> SubmitOrder(VMOrderHeader dataHeader)
        {
            //int IdCostumer = HttpContext.Session.GetInt32("IdCostumer") ?? 0;
            dataHeader.IdCostumer = IdUser;

            VMResponse respon = await orderService.SubmitOrder(dataHeader);
            return Json(respon);
        }
        public async Task <IActionResult> HistoryOrder(VMSearchPage dataSearch)
        {
            //int IdCostumer = HttpContext.Session.GetInt32("IdCostumer") ?? 0;
            int IdCostumer = IdUser;
            List<VMOrderHeader> data = await orderService.ListHeaderDetails(IdCostumer);
            int count = await orderService.CountTransaction(IdCostumer);
            ViewBag.Count = count;

            dataSearch.MinDate = dataSearch.MinDate == null ? DateTime.MinValue : dataSearch.MinDate;
            dataSearch.MaxDate = dataSearch.MaxDate == null ? DateTime.MaxValue : dataSearch.MaxDate;

            dataSearch.MinAmount = dataSearch.MinAmount == null ? decimal.MinValue : dataSearch.MinAmount;
            dataSearch.MaxAmount = dataSearch.MaxAmount == null ? decimal.MaxValue : dataSearch.MaxAmount;

            if (!string.IsNullOrEmpty(dataSearch.CodeTransaction))
            {
                data = data.Where(a => a.CodeTransaction.ToLower()
                .Contains(dataSearch.CodeTransaction.ToLower())).ToList();
            }

            if (dataSearch.MinDate != null && dataSearch.MaxDate != null)
            {
                data = data.Where ( a => a.CreateDate >= dataSearch.MinDate && a.CreateDate <= dataSearch.MaxDate).ToList();
            }

            if (dataSearch.MinAmount != null && dataSearch.MaxAmount != null)
            {
                data = data.Where(a => a.Amount >= dataSearch.MinAmount && a.Amount <= dataSearch.MaxAmount).ToList();
            }

            ViewBag.search = dataSearch;

            return View(data);
        }
        public IActionResult Search()
        {
            return PartialView();
        }

    }
}
