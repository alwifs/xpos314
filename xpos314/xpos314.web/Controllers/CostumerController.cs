﻿using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;
using xpos314.web.Service;
using xpos314.web.Services;

namespace xpos314.web.Controllers
{
	public class CostumerController : Controller
	{
		private CostumerService costumerService;
		private RoleService roleService;
		private int IdUser = 1;


		public CostumerController(RoleService _roleService, CostumerService _costumerService)
		{
			this.roleService = _roleService;
			this.costumerService = _costumerService;

			//this.configuration = _configuration;
			//this.categoryServices = new CategoryServices(this.configuration);
			//this.variantServices = new VariantServices(this.configuration);
		}
		public async Task<IActionResult> Index(string sortOrder,
											   string searchString,
											   string currentFilter,
											   int? pageNumber,
											   int? pageSize)
		{
			ViewBag.CurrentSort = sortOrder;
			ViewBag.CurrentPageSize = pageSize;
			ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

			if (searchString != null)
			{
				pageNumber = 1;

			}
			else
			{
				searchString = currentFilter;
			}

			ViewBag.CurrentFilter = searchString;

			List<VMCostumer> data = await costumerService.GetAllData();

			if (!string.IsNullOrEmpty(searchString))
			{
				data = data.Where(a => a.NameCostumer.ToLower().Contains(searchString.ToLower())
				|| a.RoleName.ToLower().Contains(searchString.ToLower())).ToList();
			}

			switch (sortOrder)
			{
				case "name_desc":
					data = data.OrderByDescending(a => a.NameCostumer).ToList();
					break;
				default:
					data = data.OrderBy(a => a.NameCostumer).ToList();
					break;
			}
			return View(PaginationList<VMCostumer>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
		}

		public async Task<IActionResult> Create()
		{
			VMCostumer data = new VMCostumer();

			List<TblRole> listRole = await roleService.GetAllData();
			ViewBag.ListRole = listRole;

			return PartialView(data);
		}

		[HttpPost]
		public async Task<IActionResult> Create(VMCostumer dataParam)
		{

			VMResponse respon = await costumerService.Create(dataParam);

			if (respon.Success)
			{
				return Json(new { dataRespon = respon });

			}
			return View(dataParam);
		}

		public async Task<IActionResult> Edit(int id)
		{
			VMCostumer data = await costumerService.GetDataById(id);

			List<TblRole> listRole = await roleService.GetAllData();
			ViewBag.listRole = listRole;

			return PartialView(data);

		}

		[HttpPost]
		public async Task<IActionResult> Edit(VMCostumer dataParam)
		{
			VMResponse respon = await costumerService.Edit(dataParam);

			if (respon.Success)
			{
				return Json(new { dataRespon = respon });
			}
			return View(dataParam);
		}

		public async Task<IActionResult> Delete(int id)
		{
			VMCostumer data = await costumerService.GetDataById(id);
			return PartialView(data);
		}

		[HttpPost]
		public async Task<IActionResult> SureDelete(int id)
		{

			VMResponse respon = await costumerService.Delete(id);

			if (respon.Success)
			{
				return RedirectToAction("Index");
			}

			return RedirectToAction("Index", id);
		}
		public async Task<IActionResult> Detail(int id)
		{
			VMCostumer data = await costumerService.GetDataById(id);
			return PartialView(data);
		}
	}
}
