﻿using Newtonsoft.Json;
using System.Text;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
    public class ProductService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public ProductService(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }
        public async Task<List<VMTblProduct>> GetAllData()
        {
            List<VMTblProduct> data = new List<VMTblProduct>();
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiProduct/GetAllData");

            data = JsonConvert.DeserializeObject<List<VMTblProduct>>(apiResponse)!;

            return data;
        }
        public async Task<VMResponse> Create(VMTblProduct dataParam)
        {
            // proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);
            /// proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            // proses memanggil api dan pengiriman body
            var request = await client.PostAsync(RouteAPI + "apiProduct/Save", content);


            if (request.IsSuccessStatusCode)
            {
                // proses membaca respon api
                var apiRespon = await request.Content.ReadAsStringAsync();

                // proses 
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;

            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }
        public async Task<VMTblProduct> GetDataById(int id)
        {
            VMTblProduct data = new VMTblProduct();
            string apiRespon = await client.GetStringAsync(RouteAPI + $"apiProduct/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<VMTblProduct>(apiRespon)!;

            return data;
        }

        public async Task<VMResponse> Edit(VMTblProduct dataParam)
        {
            // proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);
            /// proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            // proses memanggil api dan pengiriman body
            var request = await client.PutAsync(RouteAPI + "apiProduct/Edit", content);


            if (request.IsSuccessStatusCode)
            {
                // proses membaca respon api
                var apiRespon = await request.Content.ReadAsStringAsync();

                // proses 
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;

            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

		public async Task<VMResponse> Delete(int id)
		{
			var request = await client.DeleteAsync(RouteAPI + $"apiProduct/Delete/{id}");

			if (request.IsSuccessStatusCode)
			{
				// proses membaca respon api
				var apiRespon = await request.Content.ReadAsStringAsync();

				// proses 
				respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;

			}
			else
			{
				respon.Success = false;
				respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
			}

			return respon;
		}

	}
}
