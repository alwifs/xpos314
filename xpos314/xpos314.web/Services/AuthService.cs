﻿using Newtonsoft.Json;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
    public class AuthService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public AuthService(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }
        public async Task<VMCostumer> CheckLogin(string email, string password)
        {
            VMCostumer data = new VMCostumer();

            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiAuth/CheckLogin/{email}/{password}");

            data = JsonConvert.DeserializeObject<VMCostumer>(apiResponse)!;

            return data;
        }
    }
}
