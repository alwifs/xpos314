﻿using Newtonsoft.Json;
using System.Text;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
    public class RoleService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public RoleService(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }

        public async Task<List<TblRole>> GetAllData()
        {
            List<TblRole> data = new List<TblRole>();
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiRole/GetAllData");

            data = JsonConvert.DeserializeObject<List<TblRole>>(apiResponse)!;

            return data;
        }
        public async Task<VMResponse> Create(TblRole dataParam)
        {
            // proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);
            /// proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            // proses memanggil api dan pengiriman body
            var request = await client.PostAsync(RouteAPI + "apiRole/Save", content);


            if (request.IsSuccessStatusCode)
            {
                // proses membaca respon api
                var apiRespon = await request.Content.ReadAsStringAsync();

                // proses 
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;

            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<bool> CheckCategoryByName(string roleName)
        {
            string apiRespon = await client.GetStringAsync(RouteAPI + $"apiRole/CheckCategoryByName/ {roleName}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apiRespon)!;
            return isExist;
        }

        public async Task<TblRole> GetDataById(int id)
        {
            TblRole data = new TblRole();
            string apiRespon = await client.GetStringAsync(RouteAPI + $"apiRole/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<TblRole>(apiRespon)!;

            return data;
        }

        public async Task<VMResponse> Edit(TblRole dataParam)
        {
            // proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);
            /// proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            // proses memanggil api dan pengiriman body
            var request = await client.PutAsync(RouteAPI + "apiRole/Edit", content);


            if (request.IsSuccessStatusCode)
            {
                // proses membaca respon api
                var apiRespon = await request.Content.ReadAsStringAsync();

                // proses 
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;

            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMResponse> Delete(int id, int createBy)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiRole/Delete/{id}/{createBy}");

            if (request.IsSuccessStatusCode)
            {
                // proses membaca respon api
                var apiRespon = await request.Content.ReadAsStringAsync();

                // proses 
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;

            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }
        public async Task<VMResponse> Detail(TblRole dataParam)
        {
            // proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);
            /// proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            // proses memanggil api dan pengiriman body
            var request = await client.PutAsync(RouteAPI + "apiRole/Detail", content);


            if (request.IsSuccessStatusCode)
            {
                // proses membaca respon api
                var apiRespon = await request.Content.ReadAsStringAsync();

                // proses 
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;

            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }
    }
}
