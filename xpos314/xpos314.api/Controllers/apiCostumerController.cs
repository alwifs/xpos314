﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.api.Controllers
{
	[Route("[controller]")]
	[ApiController]
	public class apiCostumerController : ControllerBase
	{
		private readonly XPOS_314Context db;
		private VMResponse respon = new VMResponse();
		private int IdUser = 1;

		public apiCostumerController(XPOS_314Context _db)
		{
			this.db = _db;
		}

		[HttpGet("GetAllData")]
		public List<VMCostumer> GetAllData()
		{
			List<VMCostumer> data = (from c in db.TblCostumers
									 join r in db.TblRoles on c.IdRole equals r.Id
									 where c.IsDelete == false
									 select new VMCostumer
									 {
										 Id = c.Id,
										 NameCostumer = c.NameCostumer,
										 Email = c.Email,
										 Password = c.Password,
										 Address = c.Address,
										 Phone = c.Phone,

										 IdRole = c.IdRole,
										 RoleName = r.RoleName
									 }).ToList();
			return data;
		}
		[HttpGet("GetDataById/{id}")]
		public VMCostumer GetDataById(int id)
		{
			VMCostumer data = (from c in db.TblCostumers
							   join r in db.TblRoles on c.IdRole equals r.Id
							   where c.IsDelete == false && c.Id == id
							   select new VMCostumer
							   {
								   Id = c.Id,
								   NameCostumer = c.NameCostumer,
								   Email = c.Email,
								   Password = c.Password,
								   Address = c.Address,
								   Phone = c.Phone,

								   IdRole = c.IdRole,
								   RoleName = r.RoleName
							   }).FirstOrDefault()!;
			return data;
		}

		[HttpGet("GetDataByIdRole/{id}")]
		public List<VMCostumer> GetDataByIdRole(int id)
		{
			List<VMCostumer> data = (from c in db.TblCostumers
									 join r in db.TblRoles on c.IdRole equals r.Id
									 where c.IsDelete == false && c.IdRole == id
									 select new VMCostumer
									 {
										 Id = c.Id,
										 NameCostumer = c.NameCostumer,
										 Email = c.Email,
										 Password = c.Password,
										 Address = c.Address,
										 Phone = c.Phone,

										 IdRole = c.IdRole,
										 RoleName = r.RoleName
									 }).ToList();
			return data;
		}

		[HttpPost("Save")]
		public VMResponse Save(TblCostumer data)
		{
			data.CreateBy = IdUser;
			data.CreateDate = DateTime.Now;
			data.IsDelete = false;

			try
			{
				db.Add(data);
				db.SaveChanges();

				respon.Message = "Data success saved";
			}
			catch (Exception e)
			{
				respon.Success = false;
				respon.Message = "Failed saved : " + e.Message;
			}
			return respon;
		}
		[HttpPut("Edit")]
		public VMResponse Edit(TblCostumer data)
		{
			TblCostumer dt = db.TblCostumers.Where(a => a.Id == data.Id).FirstOrDefault()!;

			if (dt != null)
			{
				dt.NameCostumer = data.NameCostumer;
				dt.Email = data.Email;
				dt.Password = data.Password;
				dt.Address = data.Address;
				dt.Phone = data.Phone;
				dt.IdRole = data.IdRole;
				dt.UpdateBy = IdUser;
				dt.UpdateDate = DateTime.Now;

				try
				{
					db.Update(dt);
					db.SaveChanges();

					respon.Message = "Data Success saved";
				}
				catch (Exception e)
				{
					respon.Success = false;
					respon.Message = "Failed saved : " + e.Message;
				}
			}
			else
			{
				respon.Success = false;
				respon.Message = "Data Not Found";
			}
			return respon;
		}

		[HttpDelete("Delete/{id}")]
		public VMResponse Delete(int id)
		{
			TblCostumer dt = db.TblCostumers.Where(a => a.Id == id).FirstOrDefault()!;

			if (dt != null)
			{
				dt.IsDelete = true;
				dt.UpdateBy = IdUser;
				dt.UpdateDate = DateTime.Now;

				try
				{
					db.Update(dt);
					db.SaveChanges();

					respon.Message = $"Data {dt.NameCostumer} success deleted";
				}
				catch (Exception e)
				{
					respon.Success = false;
					respon.Message = "Delete Failed : " + e.Message;
				}
			}
			else
			{
				respon.Success = false;
				respon.Message = "Data not found";
			}
			return respon;
		}
	}
}
