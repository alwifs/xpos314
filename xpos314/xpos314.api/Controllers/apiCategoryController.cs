﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiCategoryController : ControllerBase
    {
        private readonly XPOS_314Context db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public apiCategoryController(XPOS_314Context _db)
        {
            this.db = _db;
        }

        [HttpGet("GetAllData")]
        public List<TblCategory> GetAllData()
        {
            List<TblCategory> data = db.TblCategories.Where(a => a.IsDelete == false).ToList();
            return data;

        }
        [HttpGet("GetDataById/{id}")]
        public TblCategory DataById(int id)
        {
        TblCategory result = new TblCategory();
        result = db.TblCategories.Where(a => a.Id == id).FirstOrDefault()!;
        return result;
        }
        [HttpGet("CheckCategoryByName/{name}")]
        public bool CheckName(string name)
        {
            TblCategory data = db.TblCategories.Where(a => a.NameCategory == name).FirstOrDefault()!;
            
            if (data != null)
            {
                return true;

            }
            else
            {
                return false;
            }
            //return false; opsi langsung
        }

        [HttpPost("Save")]
        public VMResponse Save(TblCategory data)
        {

            data.CreateBy = IdUser;
            data.CreateDate= DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data); db.SaveChanges();

                respon.Message = "Data succses saved";
            }
            catch (Exception e)
            {
                respon.Success = false;
                respon.Message = "Failed Saved" + e.Message;
            }
            return respon;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(TblCategory data)
        {
            TblCategory dta = db.TblCategories.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dta != null)
            {
               
                dta.NameCategory = data.NameCategory;
                dta.Description = data.Description;
                dta.UpdateBy = IdUser;
                dta.UpdateDate = DateTime.Now;
                
                try
                {
                    db.Update(dta); db.SaveChanges();

                    respon.Message = "Data Succes Update";
                }
                catch (Exception e)
                {
                    respon.Success = false;
                    respon.Message = "Data Failed" + e.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data Not Found";
            }
            return respon;
        }

        [HttpDelete("Delete/{id}/{createBy}")]
        public VMResponse Delete(int id, int createBy)
        {
            TblCategory dta = db.TblCategories.Where(a => a.Id == id).FirstOrDefault()!;

            if (dta != null)
            {
                dta.IsDelete = true;
                dta.UpdateBy = createBy;
                dta.UpdateDate = DateTime.Now;

                try
                {
                    db.Update(dta);
                    db.SaveChanges();

                    respon.Message = $"Data {dta.NameCategory} success deleted";
                }
                catch (Exception e)
                {
                    respon.Success = false;
                    respon.Message = "Delete Failed" + e.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not Found";
            }
            return respon;
        }

    }
    
}
