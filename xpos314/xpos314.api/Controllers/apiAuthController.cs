﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.api.Controllers
{
	[Route("[controller]")]
	[ApiController]
	public class apiAuthController : ControllerBase
	{
		private readonly XPOS_314Context db;
		private VMResponse respon = new VMResponse();
		private int IdUser = 1;

		public apiAuthController(XPOS_314Context _db)
		{
			this.db = _db;
		}

		[HttpGet("CheckLogin/{email}/{password}")]
		public VMCostumer CheckLogin(string email, string password)
		{
			VMCostumer data = (from c in db.TblCostumers
							   join r in db.TblRoles on c.CostumerId equals r.Id
							   where c.IsDelete == false && c.Email == email && c.Password == password
							   select new VMCostumer
							   {
								   Id = c.Id,
								   NameCostumer = c.NameCostumer,
								   IdRole = c.IdRole,
								   RoleName = r.RoleName

							   }).FirstOrDefault()!;
			return data;
		}
	}
}
