﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.api.Controllers
{
	[Route("[controller]")]
	[ApiController]
	public class apiRoleController : ControllerBase
	{
		private readonly XPOS_314Context db;
		private VMResponse respon = new VMResponse();
		private int IdUser = 1;

		public apiRoleController(XPOS_314Context _db)
		{
			this.db = _db;
		}

		[HttpGet("GetAllData")]
		public List<TblRole> GetAllData()
		{
			List<TblRole> data = db.TblRoles.Where(a => a.IsDelete == false).ToList();
			return data;

		}
		[HttpGet("GetDataById/{id}")]
		public TblRole DataById(int id)
		{
			TblRole result = new TblRole();
			result = db.TblRoles.Where(a => a.Id == id).FirstOrDefault()!;
			return result;
		}
		[HttpGet("CheckCategoryByName/{name}")]
		public bool CheckName(string name)
		{
			TblRole data = db.TblRoles.Where(a => a.RoleName == name).FirstOrDefault()!;

			if (data != null)
			{
				return true;

			}
			else
			{
				return false;
			}
			//return false; opsi langsung
		}

		[HttpPost("Save")]
		public VMResponse Save(TblRole data)
		{

			data.CreateBy = IdUser;
			data.CreateDate = DateTime.Now;
			data.IsDelete = false;

			try
			{
				db.Add(data); db.SaveChanges();

				respon.Message = "Data succses saved";
			}
			catch (Exception e)
			{
				respon.Success = false;
				respon.Message = "Failed Saved" + e.Message;
			}
			return respon;
		}

		[HttpPut("Edit")]
		public VMResponse Edit(TblRole data)
		{
			TblRole dta = db.TblRoles.Where(a => a.Id == data.Id).FirstOrDefault()!;

			if (dta != null)
			{

				dta.RoleName = data.RoleName;
				dta.UpdateBy = IdUser;
				dta.UpdateDate = DateTime.Now;

				try
				{
					db.Update(dta); db.SaveChanges();

					respon.Message = "Data Succes Update";
				}
				catch (Exception e)
				{
					respon.Success = false;
					respon.Message = "Data Failed" + e.Message;
				}

			}
			else
			{
				respon.Success = false;
				respon.Message = "Data Not Found";
			}
			return respon;
		}

		[HttpDelete("Delete/{id}/{createBy}")]
		public VMResponse Delete(int id, int createBy)
		{
			TblRole dta = db.TblRoles.Where(a => a.Id == id).FirstOrDefault()!;

			if (dta != null)
			{
				dta.IsDelete = true;
				dta.UpdateBy = createBy;
				dta.UpdateDate = DateTime.Now;

				try
				{
					db.Update(dta);
					db.SaveChanges();

					respon.Message = $"Data {dta.RoleName} success deleted";
				}
				catch (Exception e)
				{
					respon.Success = false;
					respon.Message = "Delete Failed" + e.Message;
				}

			}
			else
			{
				respon.Success = false;
				respon.Message = "Data not Found";
			}
			return respon;
		}
	}
}
