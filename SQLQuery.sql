
-- Create database xpos314
create database XPOS_314
use XPOS_314

create table tblKategori(
id int primary key identity (1,1),
namaKategori varchar (50) not null,
Description varchar (100) null,
IsDelete bit null,
CreateBy int not null,
CreateDate datetime not null,
UpdateBy int null,
UpdateDate datetime null
)

create table TblProduct(
id int primary key identity (1,1),
IdVariant int not null,
NameProduct Varchar (100) not null,
Price decimal (18,4) not null,
Stock int not null,
Image varchar (max) null,
IsDelete bit null,
CreateBy int not null,
CreateDate datetime not null,
UpdateBy int null,
UpdateDate datetime null
)

insert into TblProduct values 
--(5, 'Es Kelapa', 10000,10, 'eskelapa.jpg', 0,1, getdate(), null, null),
(12, 'Es Teller', 12000, 10, 'esteller.jpg', 0,1, getdate(), null, null)
(7, 'Salalauak Sumbar', 10000, 10, 'salalauak.jpg', 0,1, getdate(), null, null),
(5, 'Es Kelapa', 10000, 10, 'eskelapa.jpg', 0,1, getdate(), null, null),



drop
select * from TblProduct
select * from TblVariant

delete from TblProduct where id=5